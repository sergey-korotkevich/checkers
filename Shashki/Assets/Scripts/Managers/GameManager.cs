using CheckerLogic;
using UnityEngine;
using UnityEngine.UI;

namespace Managers
{
    public class GameManager : MonoBehaviour
    {
        [SerializeField] private CheckerMovement _checkerMovement;
    
        [SerializeField] private CheckerTableGenerator _checkerTableGenerator;
        
        [SerializeField] private CheckerSelecter _checkerSelecter;
    
        [SerializeField] private Text _text = default;
        
        [SerializeField] private GameObject _UI = default;

        [SerializeField] private Material whiteQueenMaterial;
        [SerializeField] private Material BlackQueenMaterial;
        
        public bool IsWhite { get; private set; }

        public bool IsWhiteTurn { get; private set; }

        private void Start()
        {
            IsWhiteTurn = true;
            IsWhite = true;
        }
    
        public void EndTurn()
        {
            int x = (int) _checkerMovement.EndPoint.x;
            int y = (int) _checkerMovement.EndPoint.y;
        
            for (int i = 0; i < 8; i++)
            {
                for (int j = 0; j < 8; j++)
                {
                    var checker = _checkerTableGenerator.Checkers[i,j];
                    if (checker != null)
                    {
                        _checkerTableGenerator.Checkers[i, j].FoundedCheckers = new Checker[4];
                        _checkerTableGenerator.Checkers[i, j].StartX = i;
                        _checkerTableGenerator.Checkers[i, j].StartY = j;
                    }
                } 
            }
        
            if (_checkerSelecter.SelectedChecker != null)
            {
                MeshRenderer selectedCheckerMeshRenderer = _checkerSelecter.SelectedChecker.gameObject.GetComponent<MeshRenderer>();
               
                if (_checkerSelecter.SelectedChecker.IsWhite && !_checkerSelecter.SelectedChecker.isQueen && y == 7)
                {
                    _checkerSelecter.SelectedChecker.transform.rotation = new Quaternion(0f,180f,0f,0);
                    selectedCheckerMeshRenderer.material = whiteQueenMaterial;
                    _checkerSelecter.SelectedChecker.isQueen = true;
                }
                else if (!_checkerSelecter.SelectedChecker.IsWhite && !_checkerSelecter.SelectedChecker.isQueen && y == 0)
                {
                    _checkerSelecter.SelectedChecker.transform.rotation = new Quaternion(0f,180f,0f,0);
                    selectedCheckerMeshRenderer.material = BlackQueenMaterial;
                    _checkerSelecter.SelectedChecker.isQueen = true;
                }
            }
            _checkerSelecter.SelectedChecker = null;
            _checkerMovement.StartPoint = Vector2.zero;

            if (_checkerMovement.ScanningPossibleMoves(x, y).Count != 0 && _checkerMovement.hasKilled)
            {
                _checkerMovement.hasKilled = false;
                return;
            }
            _checkerTableGenerator.ForcedCheckers.Clear();

            IsWhiteTurn = !IsWhiteTurn;
            IsWhite = !IsWhite;
            _checkerMovement.hasKilled = false;
            CheckVictory();
        }
        private void CheckVictory()
        {
            bool whiteWin = true;
            bool blackWin = true;
        
            var checkers = FindObjectsOfType<Checker>();
            foreach (var checker in checkers)
            {
                checker.iCanMove = false;
                checker.MoveCheck(_checkerTableGenerator.Checkers);
                if (checker.iCanMove && checker.IsWhite)
                {
                    blackWin = false;
                }
                if (checker.iCanMove && !checker.IsWhite)
                {
                    whiteWin = false;
                }
            }
            if (blackWin)
            {
                _text.text = "Черные победили";
                _UI.gameObject.SetActive(true);
            }

            if (!whiteWin) return;
            _text.text = "Белые победили";
            _UI.gameObject.SetActive(true);
        }
    }
}
