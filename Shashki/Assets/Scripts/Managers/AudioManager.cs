﻿using UnityEngine;

namespace Managers
{
    public class AudioManager : MonoBehaviour
    {
        [SerializeField] private AudioSource _audioSource = default;

        private static AudioManager _instance;

        public static AudioManager Instance
        {
            get
            {
                if (_instance != null)
                {
                    return _instance;
                }

                return _instance;
            }
        }

        private void Awake()
        {
            if (_instance != null)
            {
                Destroy(gameObject);
                return;
            }
            _instance = this;
            DontDestroyOnLoad(gameObject);
        }
    
        public void PlayAudio(AudioClip audioClip)
        {
            _audioSource.PlayOneShot(audioClip);
        }
    }
}