using Managers;
using UnityEngine;

namespace CheckerLogic
{
    public class CheckerSelecter : MonoBehaviour
    {
        [SerializeField] private CheckerTableGenerator _checkerTableGenerator;
    
        [SerializeField] private CheckerMovement _checkerMovement;
    
        [SerializeField] private GameManager _gameManager;

        private Checker selectedChecker = default;

        public Checker SelectedChecker
        {
            get => selectedChecker;
            set => selectedChecker = value;
        }

        public void SelectChecker(int x, int y)
        {
            if (x == -1 || y == -1)
            {
                return;
            }
            Checker checker = _checkerTableGenerator.Checkers[x, y];

            if (x < 0 || x >= 8 || y < 0 ||
                y >= 8)
            {
                return;
            }

            if (checker != null && checker.IsWhite == _gameManager.IsWhite)
            {
                if (_checkerTableGenerator.ForcedCheckers.Count == 0)
                {
                    selectedChecker = checker;
                    _checkerMovement.StartPoint = _checkerMovement.MouseOver;
                }
                else
                {
                    if (_checkerTableGenerator.ForcedCheckers
                        .Find(fchecker => fchecker == checker) == null)
                        return;
                    selectedChecker = checker;
                    _checkerMovement.StartPoint = _checkerMovement.MouseOver;            }
            }
        }
    }
}
