using System.Collections.Generic;
using UnityEngine;

namespace CheckerLogic
{
    public class CheckerTableGenerator : MonoBehaviour
    {
        [SerializeField] private GameObject _whiteCheckerPrefab = default;
        [SerializeField] private GameObject _blackCheckerPrefab = default;

        [SerializeField] private readonly Checker[,] _checkers = new Checker[8, 8];

        public List<Checker> ForcedCheckers { get; set; } = new List<Checker>();

        public Checker[,] Checkers => _checkers;

        private void Start()
        {
            ForcedCheckers = new List<Checker>();
            GenerateBoard();
        }
    
        private void GenerateBoard()
        {
            for (var y = 0; y < 3; y++)
            {
                var oddRow = y % 2 == 0;
                for (var x = 0; x < 8; x += 2)
                {
                    GenerateChecker(_whiteCheckerPrefab,oddRow ? x  : x + 1, y);
                }
            } 
        
            for (var y = 7; y > 4; y--)
            {
                var oddRow = y % 2 == 0;
                for (var x = 0; x < 8; x += 2)
                {
                    GenerateChecker(_blackCheckerPrefab,oddRow ? x  : x + 1, y);
                }
            }
        }

        private void GenerateChecker(GameObject checkerPrefab, int x, int y)
        {
            GameObject generatedChecker = Instantiate(checkerPrefab);
            Checker checker = generatedChecker.GetComponent<Checker>();
        
            checker.transform.parent = transform;
            _checkers[x, y] = checker;
            
            CheckerMovement.MoveChecker(checker,x ,y);
        }
    }
}