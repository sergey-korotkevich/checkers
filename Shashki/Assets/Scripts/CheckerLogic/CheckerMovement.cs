using System.Collections.Generic;
using Managers;
using UnityEngine;

namespace CheckerLogic
{
    public class CheckerMovement : MonoBehaviour
    {
        [SerializeField] private CheckerTableGenerator _checkerTableGenerator;
    
        [SerializeField] private GameManager _gameManager;
    
        [SerializeField] private CheckerSelecter _checkerSelecter;
    
        [SerializeField] private AudioClip _checkerMoveSound;
        [SerializeField] private AudioClip _checkerDestroySound;
    
        public bool hasKilled;
    
        private static readonly Vector3 _offset = new Vector3(-7f, 0.2f, -7f);
        private static readonly Vector3 _boardOffset = new Vector3(-7f, 0f, -7f);

        private Vector2 _startPoint = default;
        private Vector2 _endPoint = default;
        private Vector2 _mouseOver = default;
        
        public Vector2 StartPoint
        {
            set => _startPoint = value;
        }

        public Vector2 EndPoint => _endPoint;

        public Vector2 MouseOver => _mouseOver;

        private void Update()
        {
            UpdateMouseOver();

            if (_gameManager.IsWhite ? !_gameManager.IsWhiteTurn : _gameManager.IsWhiteTurn) return;
            var x = (int) _mouseOver.x;
            var y = (int) _mouseOver.y;
            
            if (x == -1 || y == -1)
            {
                return;
            }
            
            if (Input.GetMouseButtonDown(0) && _checkerSelecter.SelectedChecker != null)
            {
                MoveChecker((int) _startPoint.x, (int) _startPoint.y, x, y);
            }
            else if (Input.GetMouseButtonDown(0))
            {
                _checkerSelecter.SelectChecker(x, y);
            }
        }
    
        private void UpdateMouseOver()
        {
            if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition),
                out var raycastHit, 50.0f, LayerMask.GetMask("Board")))
            {
                _mouseOver.x = (int) (raycastHit.point.x - _boardOffset.x) / 2f;
                if (_mouseOver.x - (int) _mouseOver.x == 0.5)
                {
                    _mouseOver.x += 0.5f;
                }

                _mouseOver.y = (int) (raycastHit.point.z - _boardOffset.x) / 2f;
                if (_mouseOver.y - (int) _mouseOver.y == 0.5)
                {
                    _mouseOver.y += 0.5f;
                }
            }
            else
            {
                _mouseOver.x = -1;
                _mouseOver.y = -1;
            }
        }
        
        private void MoveChecker(int x1, int y1, int x2, int y2)
        {
            _checkerTableGenerator.ForcedCheckers = ScanningPossibleMoves();
            _startPoint = new Vector2(x1, y1);
            _endPoint = new Vector2(x2, y2);

            _checkerSelecter.SelectedChecker = _checkerTableGenerator.Checkers[x1, y1];
            if (_endPoint == _startPoint)
            {
                AudioManager.Instance.PlayAudio(_checkerMoveSound);
                MoveChecker(_checkerSelecter.SelectedChecker, x1, y1);
                _startPoint = Vector2.zero;
                _checkerSelecter.SelectedChecker = null;
                return;
            }
        
            if (_checkerSelecter.SelectedChecker.MoveCheck(_checkerTableGenerator.Checkers, x1, y1, x2, y2))
            {
                if (_checkerSelecter.SelectedChecker.IsFounded && _checkerSelecter.SelectedChecker.isQueen)
                {
                    QueenMovesScanner(x1,y1,x2,y2);
                }
            
                if (Mathf.Abs(x2 - x1) == 2 && !_checkerSelecter.SelectedChecker.isQueen)
                {
                    Checker checker = _checkerTableGenerator.Checkers[(x1 + x2) / 2, (y1 + y2) / 2];
                    if (checker != null)
                    {
                        _checkerTableGenerator.Checkers[(x1 + x2) / 2, (y1 + y2) / 2] = null;
                        checker.gameObject.SetActive(false);
                        AudioManager.Instance.PlayAudio(_checkerDestroySound);
                        hasKilled = true;
                    }
                }
            
                if (_checkerTableGenerator.ForcedCheckers.Count != 0 && !hasKilled) 
                {
                    MoveChecker(_checkerSelecter.SelectedChecker, x1, y1);
                    _startPoint = Vector2.zero;
                    _checkerSelecter.SelectedChecker = null;
                    return;
                }
            
                _checkerTableGenerator.Checkers[x2, y2] = _checkerSelecter.SelectedChecker;
                _checkerTableGenerator.Checkers[x1, y1] = null;
                
                AudioManager.Instance.PlayAudio(_checkerMoveSound);
                MoveChecker(_checkerSelecter.SelectedChecker, x2, y2);
            
                _gameManager.EndTurn();
            }
            else
            {
                MoveChecker(_checkerSelecter.SelectedChecker, x1, y1);
                _startPoint = Vector2.zero;
                _checkerSelecter.SelectedChecker = null;
            }
        }

        private void QueenMovesScanner(int x1,int y1, int x2, int y2)
        {
            Checker checker;
            
            if (_checkerTableGenerator.Checkers[x1, y1].FoundedCheckers[3] != null)
            {
                if(x2 > _checkerTableGenerator.Checkers[x1, y1].FoundedCheckers[3].StartX 
                   && y2<_checkerTableGenerator.Checkers[x1, y1].FoundedCheckers[3].StartY)
                {
                    checker = _checkerTableGenerator.Checkers[x1, y1].FoundedCheckers[3];
                    if (checker != null)
                    {
                        for (int i = 0; i < 8; i++)
                        {
                            for (int j = 0; j < 8; j++)
                            {
                                if (_checkerTableGenerator.Checkers[i, j] ==
                                    _checkerTableGenerator.Checkers[x1, y1].FoundedCheckers[3])
                                {
                                    _checkerTableGenerator.Checkers[i, j] = null;
                                    AudioManager.Instance.PlayAudio(_checkerDestroySound);
                                }
                            } 
                        }
                        checker.gameObject.SetActive(false);   
                        hasKilled = true;
                        checker.FoundedCheckers = null;
                        _checkerSelecter.SelectedChecker.IsFounded = false;
                        return;
                    }
                }
            }

            if (_checkerTableGenerator.Checkers[x1, y1].FoundedCheckers[0] != null)
            {
                if(x2 > _checkerTableGenerator.Checkers[x1, y1].FoundedCheckers[0].StartX 
                   && y2 > _checkerTableGenerator.Checkers[x1, y1].FoundedCheckers[0].StartY)
                {
                    checker = _checkerTableGenerator.Checkers[x1, y1].FoundedCheckers[0];
                    if (checker != null)
                    {
                        for (int i = 0; i < 8; i++)
                        {
                            for (int j = 0; j < 8; j++)
                            {
                                if (_checkerTableGenerator.Checkers[i, j] ==
                                    _checkerTableGenerator.Checkers[x1, y1].FoundedCheckers[0])
                                {
                                    _checkerTableGenerator.Checkers[i, j] = null;
                                    AudioManager.Instance.PlayAudio(_checkerDestroySound);
                                }
                            } 
                        }
                        checker.gameObject.SetActive(false);
                        hasKilled = true;
                        checker.FoundedCheckers = null;
                        _checkerSelecter.SelectedChecker.IsFounded = false;
                        return;
                    }
                }
            }

            if (_checkerTableGenerator.Checkers[x1, y1].FoundedCheckers[2] != null)
            {
                if (x2 < _checkerTableGenerator.Checkers[x1, y1].FoundedCheckers[2].StartX && y2 < _checkerTableGenerator.Checkers[x1, y1].FoundedCheckers[2].StartY)
                {
                    checker = _checkerTableGenerator.Checkers[x1, y1].FoundedCheckers[2];
                    if (checker != null)
                    {
                        for (int i = 0; i < 8; i++)
                        {
                            for (int j = 0; j < 8; j++)
                            {
                                if (_checkerTableGenerator.Checkers[i, j] ==
                                    _checkerTableGenerator.Checkers[x1, y1].FoundedCheckers[2])
                                {
                                    _checkerTableGenerator.Checkers[i, j] = null;
                                    AudioManager.Instance.PlayAudio(_checkerDestroySound);
                                }
                            } 
                        }
                        checker.gameObject.SetActive(false);
                        hasKilled = true;
                        checker.FoundedCheckers = null;
                        _checkerSelecter.SelectedChecker.IsFounded = false;
                        return;
                    }
                }
            }

            if (_checkerTableGenerator.Checkers[x1, y1].FoundedCheckers[1] != null)
            {
                if (x2 < _checkerTableGenerator.Checkers[x1, y1].FoundedCheckers[1].StartX && y2 > _checkerTableGenerator.Checkers[x1, y1].FoundedCheckers[1].StartY)
                {
                    checker = _checkerTableGenerator.Checkers[x1, y1].FoundedCheckers[1];
                    if (checker != null)
                    {
                        for (int i = 0; i < 8; i++)
                        {
                            for (int j = 0; j < 8; j++)
                            {
                                if (_checkerTableGenerator.Checkers[i, j] ==
                                    _checkerTableGenerator.Checkers[x1, y1].FoundedCheckers[1])
                                {
                                    _checkerTableGenerator.Checkers[i, j] = null;
                                    AudioManager.Instance.PlayAudio(_checkerDestroySound);
                                }
                            } 
                        }
                        checker.gameObject.SetActive(false);
                        hasKilled = true;
                        checker.FoundedCheckers = null;
                        _checkerSelecter.SelectedChecker.IsFounded = false;
                    } 
                }
            }

            
        }
        
        public List<Checker> ScanningPossibleMoves(int x, int y)
        {
            _checkerTableGenerator.ForcedCheckers.Clear();

            if (_checkerTableGenerator.Checkers[x, y]
                .isForceMoving(_checkerTableGenerator.Checkers, x, y))
            {
                _checkerTableGenerator.ForcedCheckers.Add
                    (_checkerTableGenerator.Checkers[x, y]);
            }

            return _checkerTableGenerator.ForcedCheckers;
        }

        private List<Checker> ScanningPossibleMoves()
        {
            _checkerTableGenerator.ForcedCheckers.Clear();

            for (int i = 0; i < 8; i++)
            {
                for (int j = 0; j < 8; j++)
                {
                    if (_checkerTableGenerator.Checkers[i, j] != null &&
                        _checkerTableGenerator.Checkers[i, j].IsWhite == _gameManager.IsWhiteTurn)
                    {
                        if (_checkerTableGenerator.Checkers[i, j].isForceMoving(_checkerTableGenerator.Checkers, i, j))
                        {
                            _checkerTableGenerator.ForcedCheckers.Add(_checkerTableGenerator.Checkers[i, j]);
                        }
                    }
                }
            }
            return _checkerTableGenerator.ForcedCheckers;
        }

        public static void MoveChecker(Checker checker, float x, float y)
        {
            checker.transform.position = Vector3.right * x * 2 + Vector3.forward * (y * 2f) + _offset;
        }
    }
}