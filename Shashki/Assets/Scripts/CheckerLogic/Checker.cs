using System;
using UnityEngine;

namespace CheckerLogic
{
   public class Checker : MonoBehaviour
   {
      [SerializeField] private bool isWhite;
      [SerializeField] private bool isFounded = false;

      private int startY;
      
      public Checker[] foundedCheckers;
   
      public bool iCanMove;
   
      public Checker[] FoundedCheckers
      {
         get => foundedCheckers;
         set => foundedCheckers = value;
      }
   
      public int StartX { get; set; }

      public int StartY
      {
         get => startY;
         set => startY = value;
      }

      public bool isQueen;

      private void Start()
      {
         foundedCheckers = new Checker[4];
      }

      public bool IsFounded
      {
         get => isFounded;
         set => isFounded = value;
      }
      public bool IsWhite => isWhite;

      public bool isForceMoving(Checker[,] board, int x, int y)
      {
         if (x >= 2 && y <= 5 && !isQueen)
         {
            Checker checker = board[x - 1, y + 1];
            if (checker != null && checker.isWhite != isWhite)
            {
               if (board[x - 2, y + 2] == null)
               {
                  iCanMove = true;
                  return true;
               }
            }
         }
         if (x <= 5 && y <= 5 && !isQueen)
         {
            Checker checker = board[x + 1, y + 1];
            if (checker != null && checker.isWhite != isWhite)
            {
               if (board[x + 2, y + 2] == null)
               {
                  return true;
               }
            }
         }
         if (x >= 2 && y >= 2 && !isQueen)
         {
            Checker checker = board[x - 1, y - 1];
            if (checker != null && checker.isWhite != isWhite)
            {
               if (board[x - 2, y - 2] == null)
               {
                  return true;
               }
            }
         }
         if (x <= 5 && y >= 2 && !isQueen)
         {
            Checker checker = board[x + 1, y - 1];
            if (checker != null && checker.isWhite != isWhite)
            {
               if (board[x + 2, y - 2] == null)
               {
                  return true;
               }
            }
         }
      
         return isQueen && isQueenForceMoving(board,x,y);
      }

      private bool isQueenForceMoving(Checker[,] board, int x, int y)
      {
         if (x >= 2 && y <= 5)
         {
            for (int X1 = x, Y1 = y; X1 > 0 && Y1 < 8; X1--,Y1++)
            {
               if (board[X1, Y1] != null)
               {
                  if (board[X1, Y1].isWhite == isWhite && board[X1, Y1] != board[x, y])
                  {
                     break;
                  }
                  try
                  {
                     if (board[X1, Y1].isWhite != isWhite && board[X1 - 1, Y1 + 1] == null)
                     {
                        foundedCheckers[1] = board[X1, Y1];
                        break;
                     }

                     if (board[X1, Y1].isWhite != isWhite && board[X1 - 1, Y1 + 1] != null)
                     {
                        break;
                     }
                  }
                  catch (IndexOutOfRangeException)
                  {
                     break;
                  }
               }
            }
         }
         if (x <= 5 && y <= 5)
         {
            for (int X1 = x, Y1 = y; X1 < 8 && Y1 < 8; X1++,Y1++)
            {
               if (board[X1, Y1] != null)
               {
                  if (board[X1, Y1].isWhite == isWhite && board[X1, Y1] != board[x, y])
                  {
                     break;
                  }
                  try
                  {
                     if (board[X1, Y1].isWhite != isWhite && board[X1 + 1, Y1 + 1] == null)
                     {
                        foundedCheckers[0] = board[X1, Y1];
                        break;
                     }
                     if (board[X1, Y1].isWhite != isWhite && board[X1 - 1, Y1 + 1] != null)
                     {
                        break;
                     }
                  }
                  catch (IndexOutOfRangeException)
                  {
                     break;
                  }
               }
            }
         }
         if (x >= 2 && y >= 2)
         {
            for (int X1 = x, Y1 = y; X1 > 0 && Y1 > 0; X1--,Y1--)
            {
               if (board[X1, Y1] != null)
               {
                  if (board[X1, Y1].isWhite == isWhite && board[X1, Y1] != board[x, y])
                  {
                     break;
                  }
                  try
                  {
                     if (board[X1, Y1].isWhite != isWhite && board[X1 - 1, Y1 - 1] == null)
                     {
                        foundedCheckers[2] = board[X1, Y1];
                        break;
                     }
                     if (board[X1, Y1].isWhite != isWhite && board[X1 - 1, Y1 + 1] != null)
                     {
                        break;
                     }
                  }
                  catch (IndexOutOfRangeException)
                  {
                     break;
                  }
               }
            }
         }

         if (x <= 5 && y >= 2)
         {
            for (int X1 = x, Y1 = y; X1 < 8 && Y1 > 0; X1++,Y1--)
            {
               if (board[X1, Y1] != null)
               {
                  if (board[X1, Y1].isWhite == isWhite && board[X1, Y1] != board[x, y])
                  {
                     break;
                  }
                  try
                  {
                     if (board[X1, Y1].isWhite != isWhite && board[X1 + 1, Y1 - 1] == null)
                     {
                        foundedCheckers[3] = board[X1, Y1];
                        break;
                     }
                     if (board[X1, Y1].isWhite != isWhite && board[X1 - 1, Y1 + 1] != null)
                     {
                        break;
                     }
                  }
                  catch (IndexOutOfRangeException)
                  {
                     break;
                  }
               }
            }
         }

         foreach (var checker in foundedCheckers)
         {
            if (checker != null)
            {
               return true;
            }
         }
         
         return false;
      }
      public bool MoveCheck(Checker[,] board, int x1, int y1, int x2, int y2)
      {
         if (board[x2, y2] != null)
         {
            return false;
         }
      
         if (isQueen)
         {
            if (x2 > x1 && y2 > y1)
            {
               int encounterCount = 0;
               for (int X1 = x1, Y1 = y1; X1 < 8 && Y1 < 8; X1++,Y1++)
               {
                  
                  if (board[X1, Y1] != null && board[X1,Y1] != board[x1,y1])
                  {
                     encounterCount++;
                     
                     if (encounterCount == 2)
                     {
                        break;
                     }
                     
                     if (board[X1, Y1].isWhite == isWhite && board[X1,Y1] != board[x1,y1])
                     {
                        break;
                     }
                     if (board[X1 + 1, Y1 + 1] != null && board[X1,Y1] != board[x1,y1])
                     {
                        break;
                     }
                  }
                  if (x2 == X1 && y2 == Y1)
                  {
                     isFounded = true;
                     return true;
                  }
               }
            }
         
            if (x2 < x1 && y2 > y1)
            {
               int encounterCount = 0;
               for (int X1 = x1, Y1 = y1; X1 >= 0 && Y1 < 8; --X1,++Y1)
               {
                  if (board[X1, Y1] != null && board[X1,Y1] != board[x1,y1])
                  {
                     encounterCount++;
                     
                     if (encounterCount == 2)
                     {
                        break;
                     }
                     
                     if (board[X1, Y1].isWhite == isWhite && board[X1,Y1] != board[x1,y1])
                     {
                        break;
                     }
                     if (board[X1 - 1, Y1 + 1] != null && board[X1,Y1] != board[x1,y1])
                     {
                        break;
                     }
                  }
                  if (x2 == X1 && y2 == Y1)
                  {
                     isFounded = true;
                     return true;
                  }
               }
            }

            if (x2 < x1 && y2 < y1)
            {
               int encounterCount = 0;
               for (int X1 = x1, Y1 = y1; X1 >= 0 && Y1 >= 0; X1--,Y1--)
               {
                  if (board[X1, Y1] != null && board[X1,Y1] != board[x1,y1])
                  {
                     encounterCount++;
                     
                     if (encounterCount == 2)
                     {
                        break;
                     }
                     if (board[X1, Y1].isWhite == isWhite)
                     {
                        break;
                     }

                     if (board[X1 - 1, Y1 - 1] != null)
                     {
                        break;
                     }
                     
                  }
              
                  if (x2 == X1 && y2 == Y1)
                  {
                     isFounded = true;
                     return true;
                  }
               }
            }

            if (x2 > x1 && y2 < y1)
            {
               int encounterCount = 0;
               for (int X1 = x1, Y1 = y1; X1 < 8 && Y1 >= 0; X1++,Y1--)
               {
                  if (board[X1, Y1] != null && board[X1,Y1] != board[x1,y1])
                  {
                     encounterCount++;
                     
                     if (encounterCount == 2)
                     {
                        break;
                     }
                     
                     if (board[X1, Y1].isWhite == isWhite && board[X1,Y1] != board[x1,y1])
                     {
                        break;
                     }
                     if (board[X1 + 1, Y1 - 1] != null && board[X1,Y1] != board[x1,y1])
                     {
                        break;
                     }
                  }
                  if (x2 == X1 && y2 == Y1)
                  {
                     isFounded = true;
                     return true;
                  }
               }
            }
         }
      
         int moveDestinationX = Mathf.Abs(x1 - x2);
         int moveDestinationY = y1 - y2;
      
         if (!isWhite && !isQueen)
         {
            if (moveDestinationX == 1)
            {
               if (moveDestinationY == 1)
               {
                  return true;
               }
            }
            else if (moveDestinationX == 2 || moveDestinationX == -2)
            {
               if (moveDestinationY == 2 || moveDestinationY == -2)
               {
                  Checker checker = board[(x1 + x2) / 2, (y1 + y2) / 2];
                  if (checker != null && checker.isWhite != isWhite)
                  {
                     return true;
                  }
               } 
            }
         }
      
         if (isWhite && !isQueen)
         {
         
            if (moveDestinationX == 1)
            {
               if (moveDestinationY == -1)
               {
                  return true;
               }
            }
            else if (moveDestinationX == 2 || moveDestinationX == -2)
            {
               if (moveDestinationY == -2 || moveDestinationY == 2)
               {
                  Checker checker = board[(x1 + x2) / 2, (y1 + y2) / 2];
                  if (checker != null && checker.isWhite != isWhite)
                  {
                     return true;
                  }
               } 
            }
         }
      
         return false;
      }

      public void MoveCheck(Checker[,] board)
      {
         int x1 = StartX;
         int y1 = startY;

         if (isQueen)
         {
            for (int X1 = x1, Y1 = y1; X1 < 8 && Y1 < 8; X1++, Y1++)
            {
               if (board[X1, Y1] != null)
               {
                  if (board[X1, Y1].isWhite == isWhite && board[X1, Y1] != board[x1, y1])
                  {
                     break;
                  }
               }

               if (board[X1, Y1] == null)
               {
                  iCanMove = true;
                  return;
               }
            }

            for (int X1 = x1, Y1 = y1; X1 >= 0 && Y1 < 8; --X1, ++Y1)
            {
               if (board[X1, Y1] != null)
               {
                  if (board[X1, Y1].isWhite == isWhite && board[X1, Y1] != board[x1, y1])
                  {
                     break;
                  }
               }

               if (board[X1, Y1] == null)
               {
                  iCanMove = true;
                  return;
               }
            }
         
            for (int X1 = x1, Y1 = y1; X1 >= 0 && Y1 >= 0; X1--, Y1--)
            {
               if (board[X1, Y1] != null)
               {
                  if (board[X1, Y1].isWhite == isWhite && board[X1, Y1] != board[x1, y1])
                  {
                     break;
                  }
               }

               if (board[X1, Y1] == null)
               {
                  iCanMove = true;
                  return;
               }
            }


            for (int X1 = x1, Y1 = y1; X1 < 8 && Y1 >= 0; X1++, Y1--)
            {
               if (board[X1, Y1] != null)
               {
                  if (board[X1, Y1].isWhite == isWhite && board[X1, Y1] != board[x1, y1])
                  {
                     break;
                  }
               }

               if (board[X1, Y1] == null)
               {
                  iCanMove = true;
                  return;
               }
            }

         }
      
         if (!isWhite && !isQueen)
         {
            if (x1 - 1 > -1 && y1 -1 > -1 && board[x1 - 1, y1 - 1] == null)
            {
               iCanMove = true;
               return;
            }
            if (x1 + 1 < 8 && y1 - 1 > -1 && board[x1 + 1, y1 - 1] == null)
            {
               iCanMove = true;
               return;
            }

            if (x1 + 1 < 8 && y1 + 1 < 8 && x1 + 2 < 8 &&
                y1 + 2 < 8 && board[x1 + 1, y1 + 1] != null 
                && board[x1 + 1, y1 + 1].isWhite &&
                board[x1 + 2, y1 + 2] == null)
            {
               iCanMove = true;
               return;
            }
            
            if (x1 - 1 > -1 && y1 + 1 < 8 && x1 - 2 > -1 && y1 + 2 < 8 
                &&board[x1 - 1, y1 + 1] != null 
                && board[x1 - 1, y1 + 1].isWhite &&
                board[x1 - 2, y1 + 2] == null)
            {
               iCanMove = true;
               return;
            }
            
            if (x1 - 1 > -1 && y1 -1 > -1 && x1 - 2 > -1 && y1 - 2 > -1 
                &&board[x1 - 1, y1 - 1] != null 
                && board[x1 - 1, y1 - 1].isWhite &&
                board[x1 - 2, y1 - 2] == null)
            {
               iCanMove = true;
               return;
            }
            
            if (x1 + 1 < 8 && y1 -1 > -1 &&x1 + 2 < 8 && y1 - 2 > -1 &&
                board[x1 + 1, y1 - 1] != null 
                && board[x1 + 1, y1 - 1].isWhite &&
                board[x1 + 2, y1 - 2] == null)
            {
               iCanMove = true;
               return;
            }
         }

         if (isWhite && !isQueen)
         {

            if (x1 + 1 < 8 && y1 + 1 < 8 && board[x1 + 1, y1 + 1] == null)
            {
               iCanMove = true;
               return;
            }
            if( x1 - 1 > -1 && y1 + 1 < 8 &&board[x1 - 1, y1 + 1] == null)
            {
               iCanMove = true;
               return;
            }

            if (x1 + 1 < 8 && y1 + 1 < 8 && x1 + 2 < 8 &&
                y1 + 2 < 8 && board[x1 + 1, y1 + 1] != null 
                && !board[x1 + 1, y1 + 1].isWhite &&
                board[x1 + 2, y1 + 2] == null)
            {
               iCanMove = true;
               return;
            }
            
            if (x1 - 1 > -1 && y1 + 1 < 8 && x1 - 2 > -1 && y1 + 2 < 8 
                &&board[x1 - 1, y1 + 1] != null 
                && !board[x1 - 1, y1 + 1].isWhite &&
                board[x1 - 2, y1 + 2] == null)
            {
               iCanMove = true;
               return;
            }
            
            if (x1 - 1 > -1 && y1 -1 > -1 && x1 - 2 > -1 && y1 - 2 > -1 
                &&board[x1 - 1, y1 - 1] != null 
                && !board[x1 - 1, y1 - 1].isWhite &&
                board[x1 - 2, y1 - 2] == null)
            {
               iCanMove = true;
               return;
            }
            
            if (x1 + 1 < 8 && y1 -1 > -1 &&x1 + 2 < 8 && y1 - 2 > -1 &&
                board[x1 + 1, y1 - 1] != null 
                && !board[x1 + 1, y1 - 1].isWhite &&
                board[x1 + 2, y1 - 2] == null)
            {
               iCanMove = true;
            }

         }
      }
   }
}

