﻿using UnityEngine;
using UnityEngine.SceneManagement;

namespace Menu
{
    public class Menu : MonoBehaviour
    {
        [SerializeField] private GameObject _UI;
        public void Exit()
        {
            Application.Quit();
        }

        public void SetActive()
        {
            _UI.SetActive(!_UI.activeSelf);
        }
        public void MenuTransition()
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex != 0 ? 0 : 1);
        }

        
        public void StartGame()
        {
            SceneManager.LoadScene(1);
        }
    }
}